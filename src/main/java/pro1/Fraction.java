package pro1;

import java.util.ArrayList;

public class Fraction extends Number {

    public static Fraction parse(String stringValue)
    {
        String[] splitted = stringValue.split("/");
        splitted[0]=splitted[0].trim();
        splitted[1]=splitted[1].trim();
        return new Fraction(
            Integer.parseInt(splitted[0]),
            Integer.parseInt(splitted[1]));
    }

    public Fraction(long n, long d) {

        // spočítám gcd n,d
        // tím vydělím n,d

        if(n<0 && d<0)
        {
            long gcd = NumericUtils.gcd(-n,-d);
            N = -n / gcd;
            D = -d / gcd;
        }
        else if(n<0 && d>=0)
        {
            long gcd = NumericUtils.gcd(-n,d);
            N = n / gcd;
            D = d / gcd;
        }
        else if(n>=0 && d<0)
        {
            long gcd = NumericUtils.gcd(n,-d);
            N = -n / gcd;
            D = -d / gcd;
        }
        else if(n>=0 && d>=0)
        {
            long gcd = NumericUtils.gcd(n,d);
            N = n / gcd;
            D = d / gcd;
        }
    }

    public long N;
    public long D;

    // Potřebujeme metodu add se vstupnim parametrem Fraction a
    // vystupnim typem Fraction
    public Fraction add(Fraction other)
    {
        long citatelSouctu = Math.addExact(
                Math.multiplyExact(this.N , other.D) ,
                Math.multiplyExact(other.N , this.D) );

        long jmenovatelSouctu = Math.multiplyExact(this.D, other.D);

        return new Fraction(citatelSouctu, jmenovatelSouctu);
    }

    public boolean equals(Fraction other)
    {
        return this.N==other.N && this.D==other.D;
    }

    @Override
    public String toString() {
        return this.N + "/" + this.D;
    }

    @Override
    public int intValue() {
        int nInt = Math.toIntExact(N);
        int dInt = Math.toIntExact(D);
        return nInt / dInt;
    }

    @Override
    public long longValue() {
        return this.N / this.D;
    }

    @Override
    public float floatValue() {
        return (float)N / (float) D;
    }

    @Override
    public double doubleValue() {
        return (double) N / (double) D;
    }
}
