package pro1;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        long a,b,c,d;
        Scanner scanner = new Scanner(System.in);
//        a = scanner.nextLong();
//        b = scanner.nextLong();
//        c = scanner.nextLong();
//        d = scanner.nextLong();

        String string1 = scanner.nextLine();
        String string2 = scanner.nextLine();

        Fraction f1  = Fraction.parse(string1);
        Fraction f2  = Fraction.parse(string2);

//        Fraction f1 = new Fraction(a,b);
//        Fraction f2 = new Fraction(c,d);
        System.out.println(f1.add(f2));
        System.out.println(f1.equals(f2));

        ArrayList<Number> mojeZlomky = new ArrayList<>();
        mojeZlomky.add(f1);
        mojeZlomky.add(f2);
        mojeZlomky.add(65);
        mojeZlomky.add(65.5);
    }
}
